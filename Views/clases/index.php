<?php session_start();
if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])){ 
$usuario = $_SESSION['user_id']; 
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mis Cursos</title>
    <script src="../../public/js/jquery-3.4.1.min.js"></script>
    <script src="../../public/js/jquery.dataTables.min.js"></script>
    <script src="../../public/js/dataTables.bootstrap.min.js"></script>
    <script src="../../public/js/dataTables.buttons.min.js"></script>
    <script src="../../public/js/bootstrap.js"></script>
    <script src="../../public/js/jquery.validate.js"></script>
    <script src="../../public/js/additional-methods.js"></script>
    <script src="../../public/js/all.js"></script>
    <script src="../../public/js/sweetalert2.js"></script>
         <script src="../../public/js/popper.min.js"></script>


</head>
    <link rel="stylesheet" href="../../public/css/bootstrap.css">
    <link rel="stylesheet" href="../../public/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="../../public/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="../../public/css/all.css">
    <link rel="stylesheet" href="../../public/css/sweetalert2.css">
    <link rel="stylesheet" href="../../public/css/default.css">
    <script type="text/javascript">
    $(document).ready(function(){
        $("table#TabMisCursos").DataTable({
            "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
            },
              "oAria": {
                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            },
            "destroy":true,
            "ajax":{
              "type":"POST",
              "url":"../../views/clases/consultarMisCursos.php"
            },
            "columns":[
              {"data":"id"},
              {"data":"titulo"},
              {"data":"descripcion"},
              {"data":"Fecha_inscripcion"},
              {"defaultContent":"<button id='VerCurso' class='btn btn-info glyphicon glyphicon-facetime-video'> Ir al Curso</button>"},
            ],
        });
        //Redirecciono para que el usuario vea el curso
        $("table#TabMisCursos tbody").on("click","button#VerCurso",function(){
            var tabla =$("table#TabMisCursos").DataTable();
            var fila = tabla.row($(this).parents("tr")).data();

            var url = "http://localhost/AulaVirtual/Views/cursos/verCursoUsuario.php?curso="+fila.id_curso; 
            window.location.replace(url);
        });

          //Lista de areas  
        $.getJSON("../../views/consulta/consultarArea.php",function(datos){
            if(datos != 0){              
                    $("select#area").append("<option value='' selected>Seleccione</option>");
                    $("select#area").append("<option value='0'>TODOS LOS CURSOS</option>");
                $.each(datos,function(K,V){
                    $("select#area").append("<option value="+V['id']+">"+V['descr']+"</option>");
                });
            }else
                $("select#area").empty();
            
        });

        //Lista de Cursos segun el area
        $(document).on('change', 'select#area', function() {
            var idarea =$(this).val();
            $('select#curso').empty();

            $("select#curso").append("<option value='' selected>Seleccione</option>");
            $.getJSON("../../views/clases/consultarCursos.php",{idarea:idarea},function(datos){
                if(datos != 0){
                    $.each(datos,function(K,V){
                        $("select#curso").append("<option value="+V['id']+">"+V['descr']+"</option>");
                    });
                }else
                $("select#curso").empty();
            });
        });

        //GUARDAR CURSO AL USUARIO
        $("form#InscribirCurso").validate({
        rules : {
            area : {
                required : true,
                
            },
            curso : {
                required : true,
            },
        },
        messages : {
            area : {
                required : "El Area es Requerida",
            },
            curso : {
                required : "EL Curso es Requerido",
            },
        },
        submitHandler:function(){
            parametros = $("form#InscribirCurso").serialize();
            $.ajax({
                url : "../../views/clases/guardarCursos.php",
                type : "POST",
                data : parametros/*, id : equiposs*/,
                success: function(data){
                    console.log(data);
                    if(data==1){
                        //alert(data)
                         //transaccion exitosa
                            $("#curso").val('');
                            $("#area").val('');

                            
                            _Title = "¡Enhorabuena!";
                            _Text = "Transacción exitosa";
                            _Type = "success";

                          Swal.fire({
                                text : _Text,
                                title: _Title,
                                timer: 3000,
                                type : _Type,

                                onBeforeOpen: function (){
                                    swal.showLoading()
                                }
                            }).then((result)=>{
                                $("table#TabMisCursos").DataTable().ajax.reload();
                            });
                    }else
                    alert(data);
                }
            })
            .fail(function(){
                swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
            });        }
        });
    });
    </script>

<body>
    <?php include("../../config/menu.php");?>
    
    <div class="container-fluid">
        <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-1">
          <a href="../../views/main/" class="btn btn-primary"> Ir al Inicio</a>
        </div>      
      </div>
        <h1 class="center">Mis Cursos</h1>
        <!-- Formulario de Inscripcion al curso-->
        <div class="row">
            <div class="col-md-1"></div>     
            <div class="col-md-10">
                
                <p class="alert alert-info">Si no se Ah registrado en Ningun Curso, Presione el Siguiente Boton para Registrarse.</p>
                <p>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="glyphicon glyphicon-pencil"></i>
                     Registrarme a un Curso
                    </button>
                </p>
                <div class="collapse" id="collapseExample">
                    <div class="card card-body">
                        <div class="form-group">
                            <div class="incripcionForm">
                                <form id="InscribirCurso" method="POST">
                                    <input type="hidden" name="usuario" id="usuario" value="<?php echo $usuario ;?>">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="inputArea4">Area</label>
                                            <select id="area" name="area" class="form-control">
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="inputCurso4">Curso</label>
                                            <select id="curso" name="curso" class="form-control"></select>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-10"></div>
                                        <div class="form-group col-md-1">
                                            <button type="submit" class="btn btn-success">Guardar</button>
                                        </div>  
                                    </div>  
                                </form>
                            </div>
                        </div>
                    </div>
                </div>     
            </div>
        </div>
        <div class="row">
            <div class="col-md-1 "></div>
            <div class="col-md-10 offset-md 1">
                <table class="table table-hover table-striped center" id="TabMisCursos">
                    <thead>
                        <th class="center">N°</th>
                        <th class="center">Título</th>
                        <th class="center">Estatus</th>
                        <th class="center">Fecha de Inscripción</th>
                        <th class="center">Acción</th>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <th class="center">N°</th>
                        <th class="center">Título</th>
                        <th class="center">Estatus</th>
                        <th class="center">Fecha de Inscripción</th>
                        <th class="center">Acción</th>
                    </tfoot>
                </table>
            </div>
            <div class="col-md-1"></div>
        </div>


    </div>
</body>
</html>
<?php }else{ ?>
<div>
    <h1>
        No Se encontro ninguna session Activa por favor Valla al Login. &nbsp;&nbsp;&nbsp;
        <a href="../index.php">Login</a>
    </h1>
</div>
<?php } ?>