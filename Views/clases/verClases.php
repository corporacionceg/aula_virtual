<?php session_start();
if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id']) && isset($_REQUEST['r']) && !empty($_REQUEST['r'])){ 
$usuario = $_SESSION['user_id']; 
$curso=$_REQUEST['r'];
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ver Clases</title>
    <script src="../../public/js/jquery-3.4.1.min.js"></script>
    <script src="../../public/js/bootstrap.js"></script>
    <script src="../../public/js/all.js"></script>
    <script src="../../public/js/adminlte.js"></script>
    <!--<script src="../../public/js/additional-methods.js"></script>-->
    <script src="../../public/js/sweetalert2.js"></script>
         <script src="../../public/js/popper.min.js"></script>

    <link rel="stylesheet" href="../../public/css/adminlte.css">
    <link rel="stylesheet" href="../../public/css/all.css">
    <link rel="stylesheet" href="../../public/css/sweetalert2.css">
    <link rel="stylesheet" href="../../public/css/default.css">
    <script type="text/javascript">
    $(document).ready(function(){
        var idCurso= $("#curso").val();

        $.getJSON("../../views/clases/consultaCurso.php",{idCurso:idCurso},function(datos){
            if(datos != 0){
                $.each(datos,function(K,V){
                    $("#Contenido").append("<h1 class='center'>"+V['titulo']+"</h1>");
                    
                });
            }
        });

        $.getJSON("../../views/clases/consultarSecciones.php",{idCurso:idCurso},function(datos){
            if(datos != 0){              
                $.each(datos,function(K,V){
                    $("#Contenido").append("<div class='row'><div class='col-md-2'></div><div class='col-md-8'><div class='card'><div class='card-header'><h3 class='card-title'><i class='fas fa-text-width'></i>&nbsp;"+V['titulo']+"</h3></div><div class='card-body center' id='"+V['id_seccion']+"''>");
                        var idSeccion = V['id_seccion'];

                        $.getJSON("../../views/clases/consultaClases.php",{idCurso:idCurso,idSeccion:idSeccion},function(datos){
                            if(datos != 0){
                                i=1;
                                $.each(datos,function(K,V){
                                    $("#"+idSeccion).append("Clase N°:&nbsp;"+i+"&nbsp;&nbsp; Duración:&nbsp;"+V['duracion']+"&nbsp;&nbsp;<button type='button'  class='comentarios btn btn-xs btn-info' onclick='verClase("+V['id_clases']+")' value='"+V['clase']+"'><i class='fas fa-eye'></i>&nbsp; VER</button><br><br>");
                                    i++;
                                });
                            }
                        });
                    $("#Contenido").append("</div></div></div></div>");
                });
            }
            
        });

    });

    function verClase(valor){
            var idCurso =valor;

            $.getJSON("../../views/clases/consulClase.php",{idCurso:idCurso},function(datos){
            if(datos != 0){
                $.each(datos,function(K,V){
                    var ruta= "../../public/css/videos"+V['clase'];
                    $("#VideoClase").attr("scr",ruta);

                    $("#VideoClase").modal("show");
                    
                });
            }
        });
        }
    </script>
</head>
<body>
    <input type="hidden" name="curso" id="curso" value="<?php echo $curso; ?>">
    <?php include("../../config/menu.php"); ?>
    <div class="container-fluid" >
        <div id="Contenido"></div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="ModalVideo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nuevo Comentario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
   
                    <video id="VideoClase" controls autoplay muted loop></video>
        
                </div>
            </div>
        </div>
    </div>
    <!--end modal-->


</body>
</html>
<?php }else{ ?>
<div>
    <h1>
        No Se encontro ninguna session Activa por favor Valla al Login. &nbsp;&nbsp;&nbsp;
        <a href="index.php">Login</a>
    </h1>
</div>
<?php } ?>
