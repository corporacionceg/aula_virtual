
<!DOCTYPE html>
<html lang="es">
  <head>
    <!--<meta charset="UTF-8">-->
    <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
    <title>Publicaciones</title>
    <script src="../../public/js/jquery-3.4.1.min.js"></script>
    <script src="../../public/js/jquery.dataTables.min.js"></script>
    <script src="../../public/js/dataTables.bootstrap.min.js"></script>
    <script src="../../public/js/dataTables.buttons.min.js"></script>
    <script src="../../public/js/bootstrap.js"></script>
    <script src="../../public/js/jquery.validate.js"></script>
    <script src="../../public/js/additional-methods.js"></script>
    <script src="../../public/js/sweetalert2.js"></script>
         <script src="../../public/js/popper.min.js"></script>
    
  </head>
    <link rel="stylesheet" href="../../public/css/bootstrap.css">
    <link rel="stylesheet" href="../../public/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="../../public/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="../../public/css/default.css">
    <script>
    $(document).ready(function(){

      /*$('table#TabPublic').DataTable({  
        "bDeferRender": true,     
        "sPaginationType": "full_numbers",
        "ajax": {
          "url": "consultaPublicaciones.php",
              "type": "POST"
        },          
        "columns": [
          {"data":"id_publicacion"},
          {"data":"titulo"},
          {"data":"descripcion"},
          {"data":"comentario"},
          {"data":"fecha"},
          {"defaultContent":"<button class='comentarios btn btn-info' type='button' title='Ver'>Ver</button>"},
          //{"defaultContent":"<button class='editar btn btn-primary' type='button' title='Ver'><i class='fas fa-check-square'></i></button>"}
          ],
        
            //"columnDefs": [
            //    {
            //        "targets": [ 5 ],
            //        "visible": false,
            //    }
            //],

        "oLanguage": {
                "sProcessing":     "Procesando...",
            "sLengthMenu": 'Mostrar <select>'+
                '<option value="10">10</option>'+
                '<option value="20">20</option>'+
                '<option value="30">30</option>'+
                '<option value="40">40</option>'+
                '<option value="50">50</option>'+
                '<option value="-1">All</option>'+
                '</select> registros',    
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Filtrar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Por favor espere - cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
            }
      });*/

      $("table#TabPublic").DataTable({
      "language": {
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },
      "destroy":true,
      "ajax":{
        "type":"POST",
        /*"data":{"opcion" :1},*/
        "url":"../../views/consulta/consultaPublicaciones.php"
      },
      "columns":[
      /*
        {"defaultContent":"<span id='ok' class='glyphicon glyphicon-plus-sign text-success'>Asignar</span>"},
        {"data":"asignacion",
            "render":function(data,type,row){
                switch(data){
                    case '0'://sin asignacion
                        return "<button class='btn btn-sm btn-default'>Sin asignación</button>";
                    break;
                    default://con asignaciones
                        return "<button id='info' class='btn btn-sm btn-info'>Beneficiario</button>";
                    break;
                }
            }
        },*/
        {"data":"id_publicacion"},
        {"data":"titulo"},
        {"data":"descripcion"},
        {"data":"comentario"},
        {"data":"fecha"},
        /*
        {"data":"rol",
            "render":function(data,type,row){
                switch(data){
                    case 'SI'://sin asignacion
                        return "<span class='text-primary'>Jefe de familia</span>";
                    break;
                    case 'NO'://sin asignacion
                        return "<span class='text-danger'>integrante de familia</span>";
                    break;
                }
            }
        },*/
        {"defaultContent":"<button id='comentarios' name='comentarios[]' class='btn btn-info glyphicon glyphicon-ok'></button>"},
      ],
    });

    $("table#TabPublic tbody").on("click","button#comentarios",function(){
    var tabla =$("table#TabPublic").DataTable();
      var fila = tabla.row($(this).parents("tr")).data();
      alert(fila.id_publicacion);

    });
  });
  </script>
  <script src="../../public/js/publicacionesValidate.js"></script>
  
<body>
    <?php include("../../config/menu.php");?>
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-1"></div>
            <div class="col-md-10">
                  <div class="card card-primary card-outline">
                    <div class="card-header">
                      <h3 class="card-title">Publicaciones</h3>

                      <div class="card-tools">
                        <div class="input-group input-group-sm">
                        </div>
                      </div>
                      <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                      <div class="mailbox-controls">
                        <!-- Check all button -->
                        <!-- /.btn-group -->
                        <!-- <button type="button" class="btn btn-default btn-sm"><i class="fas fa-sync-alt"></i></button> -->
                        <div class="float-left">
                        <div class="row">
                          <div class="col-md-10"></div>
                          <div class="col-md-1">


                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#M_registrar">Agregar Publicación</button>

                          </br></br>
                          </div>
                        </div>
                          <!--1-50/200-->
                          
                          <!-- <div class="btn-group">
                            <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-left"></i></button>
                            <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-right"></i></button>
                          </div> -->
                          <!-- /.btn-group -->
                        </div>
                        <!-- /.float-right -->
                      </div>
                      <div class="table-responsive mailbox-messages">
                        <table class="table table-hover table-striped" id="TabPublic">
                          <thead>
                            <th>N°</th>
                            <th>Título</th>
                            <th>Descripcion</th>
                            <th>N° comentarios</th>
                            <th>Fecha</th>
                            <th>Acción</th>
                          </thead>
                          <tbody>
                          </tbody>
                          <tfoot>
                            <th>N°</th>
                            <th>Título</th>
                            <th>Descripcion</th>
                            <th>N° comentarios</th>
                            <th>Fecha</th>
                            <th>Acción</th>
                          </tfoot>
                        </table>
                        <!-- /.table -->
                      </div>
                      <!-- /.mail-box-messages -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer p-0">
                      <div class="mailbox-controls">
                        <!-- Check all button -->
                       <!--  <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="far fa-square"></i>
                        </button>
                        <div class="btn-group">
                          <button type="button" class="btn btn-default btn-sm"><i class="far fa-trash-alt"></i></button>
                          <button type="button" class="btn btn-default btn-sm"><i class="fas fa-reply"></i></button>
                          <button type="button" class="btn btn-default btn-sm"><i class="fas fa-share"></i></button>
                        </div> -->
                        <!-- /.btn-group -->
                        <!-- <button type="button" class="btn btn-default btn-sm"><i class="fas fa-sync-alt"></i></button> -->
                        <div class="float-right">
                          <!--1-50/200-->
                          <!-- <div class="btn-group">
                            <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-left"></i></button>
                            <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-right"></i></button>
                          </div> -->
                          <!-- /.btn-group -->
                        </div>
                        <!-- /.float-right -->
                      </div>
                    </div>
                  </div>
                  <!-- /.card -->
            </div>
        </div>
    </div>
<!-- Modal -->
<div id="M_registrar" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Nueva Publicacion</h4>
      </div>
      <div class="modal-body">
        <form id="NewPublic" method="POST" action="guardarPublicacion.php">
          <input type="hidden" name="usuario" value="1">
            <div class="row">
              <div class="col-sm-6">
                <!-- text input -->
                <div class="form-group">
                  <label>Titulo</label>
                  <input type="text" name="titulo" id="titulo" required="true" class="form-control" autofocus>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label>Decripciom</label>
                  <textarea class="form-control" required="true" name="descripcion" id="descripcion" value=""> </textarea>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6">
                <!-- select -->
                <div class="form-group">
                  <label>Curso</label>
                  <select name="curso" id="curso" class="form-control"> 
                  </select>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label>Area</label>
                  <select name="area" id="area" class="form-control">
                  </select>
                </div>
              </div>
            </div>
        </div>
            <!-- <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div> -->
        
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" id="guardar" class="btn btn-primary">Guardar</button>
        </form>
      </div>
    </div>

  </div>
</div>
<!--end modal-->

<!-- Modal 2 -->
<div id="M_comentarios" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="TituloModal" class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <table class="table table-hover table-striped" id="Public">
          <thead>
            <th>Comentario</th>
            <th>Título</th>
            <th>Usuario</th>
            <th>Fecha Registro</th>
          </thead>
          <tbody>
          </tbody>
          <tfoot>
            <th>Comentario</th>
            <th>Título</th>
            <th>Usuario</th>
            <th>Fecha Registro</th>
          </tfoot>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cerrar</button>
      </div>
    </div>

  </div>
</div>
<!--end modal-->

</body>
</html>