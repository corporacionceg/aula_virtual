<?php session_start();
if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])){ 
$usuario = $_SESSION['user_id']; 
    
?>
<!DOCTYPE html>
<html>
<head>
	<title>Publicaciones</title>
    <script src="../../public/js/popper.min.js"></script>
    <script src="../../public/js/jquery-3.4.1.min.js"></script>
    <script src="../../public/js/bootstrap.min.js"></script>
    <script src="../../public/js/jquery.validate.js"></script>
    <script src="../../public/js/additional-methods.js"></script>
    <script src="../../public/js/sweetalert2.js"></script>
    <script src="../../public/js/jquery.mask.min.js"></script>
    
    <script src="../../public/js/all.js"></script>
    <script src="../../public/js/jquery.dataTables.min.js"></script>


</head>
    <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../public/css/all.css">
    <link rel="stylesheet" href="../../public/css/sweetalert2.css">
    <link rel="stylesheet" href="../../public/css/default.css">
    <link rel="stylesheet" href="../../public/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../main/album.css">
  	<!--<link rel="stylesheet" href="../../public/css/adminlte.css">-->
  	<script>
  	$(document).ready(function(){
		$("table#TabPublic").DataTable({
			"language": {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
			  "sFirst":    "Primero",
			  "sLast":     "Último",
			  "sNext":     "Siguiente",
			  "sPrevious": "Anterior"
			},
			  "oAria": {
			      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			  }
			},
			"destroy":true,
			"ajax":{
			  "type":"POST",
			  /*"data":{"opcion" :1},*/
			  "url":"../../views/consulta/consultaPublicaciones.php"
			},
			"columns":[
			  {"data":"id_publicacion"},
			  {"data":"titulo"},
			  {"data":"descripcion"},
			  {"data":"comentario"},
			  {"data":"fecha"},
			  {"defaultContent":"<button id='comentarios' name='comentarios[]' class='btn btn-info glyphicon glyphicon-list'> VER</button>"},
			],
		});

		var tabla =$("table#TabPublic").DataTable();
    	$("table#TabPublic tbody").on("click","button#comentarios",function(){
    		var fila = tabla.row($(this).parents("tr")).data();
    		$("#TituloModal").html("COMENTARIOS DE: "+fila.titulo);
    		$("input#idPublicacion").val(fila.id_publicacion);
    		var id=fila.id_publicacion;
    		
    		getComentPublic(id)		

	      	$("#M_comentarios").modal("show");

    	});

    	function getComentPublic(id){

			  $.getJSON("../../views/consulta/consultaComentarios.php",{idPublicacion:id},function(datos){
		        if(datos != 0){
		        		var i =1;
		        	$("divt#ModelBodyComent").text("");
		        	$("#ModelBodyComent").empty();
		            $.each(datos,function(K,V){
		                $("div#ModelBodyComent").append("<div><i class='fas fa-envelope bg-blue'>"+i+"</i><div class='timeline-item'><span class='time'><i class='glyphicon glyphicon-calendar'></i> "+V['fecha']+"</span><h3 class='timeline-header'><i class='glyphicon glyphicon-user'> "+V['usuario']+"</i></h3><div class='timeline-body'>"+V['comentario']+"</div><div class='timeline-footer'></div></div></div>");
		                i++;
		            });
		        }else{
		        	$("divt#ModelBodyComent").text("");
		            $("#ModelBodyComent").empty();
		            $("div#ModelBodyComent").append("No Se Encontraron Comentarios Para esta Publicación.");
		        }
		    });
    	}


    	$("form#NewPublic").validate({
        rules : {
            titulo : {
                required : true,
                minlength:10,
                maxlength:50
                
            },
            descripcion : {
                required : true,
                minlength:10,
                maxlength:250
                
            },
        },
        messages : {
            titulo : {
                required : "El Títilo es Requerido",
                minlength:"La Cantidad Mínima es de Diez (10) Caracteres.",
                maxlength:"La Cantidad Máxima es de Cincuenta (50) Caracteres.",
                
            },
            descripcion : {
                required : "La Descripcion es Requerido",
                minlength:"La Cantidad Mínima es de Diez (10) Caracteres.",
                maxlength:"La Cantidad Máxima es de Docientos Cincuenta (250) Caracteres."
                
            },
        },
        submitHandler:function(){
            parametros = $("form#NewPublic").serialize();
            $.ajax({
                url : "guardarPublicacion.php",
                type : "POST",
                data : parametros/*, id : equiposs*/,
                success: function(data){
                    console.log(data);
                    if(data==1){
                        //alert(data)
                         //transaccion exitosa
                         	$("#exampleModal").modal("hide")
                         	$("#exampleModal").hide();
                         	$("#titulo").val('');
                         	$("#descripcion").val('');
                         	$("#curso").val('');
                         	$("#area").val('');

                            
                            _Title = "¡Enhorabuena!";
                            _Text = "Transacción exitosa";
                            _Type = "success";

                          Swal.fire({
                                text : _Text,
                                title: _Title,
                                timer: 3000,
                                type : _Type,

                                onBeforeOpen: function (){
                                    swal.showLoading()
                                }
                            }).then((result)=>{
                                /*var url = "lotes?cargar=iniciolotes";*/
                                //$(location).reload();   
                                //$("table#TblComentarios").DataTable().ajax.reload();
                                $("table#TabPublic").DataTable().ajax.reload();

                            });
                       
                    }else
                    alert(data);
                }
            })
            .fail(function(){
                swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
            });        
          }
    	});

    	$("form#NewComent").validate({
        rules : {
            comentario : {
                required : true,
                minlength:10,
                maxlength:250
                
            },
        },
        messages : {
            comentario : {
                required : "El Comentario es Requerido",
                minlength:"La Cantidad Mínima es de Diez (10) Caracteres.",
                maxlength:"La Cantidad Máxima es de Docientos Cincuenta (250) Caracteres."
                
            },
        },
        submitHandler:function(){
            parametros = $("form#NewComent").serialize();
            $.ajax({
                url : "guardarComentario.php",
                type : "POST",
                data : parametros/*, id : equiposs*/,
                success: function(data){
                    console.log(data);
                    if(data==1){
                       	$("#ModalComentario").modal("hide")
                        $("#ModalComentario").hide();
                         	
                            _Title = "¡Enhorabuena!";
                            _Text = "Transacción exitosa";
                            _Type = "success";

                          Swal.fire({
                                text : _Text,
                                title: _Title,
                                timer: 3000,
                                type : _Type,

                                onBeforeOpen: function (){
                                    swal.showLoading()
                                }
                            }).then((result)=>{

                            	var publicIdRecarg =$("#idPublicacion").val();
                            	getComentPublic(publicIdRecarg);
                                $("#ModelBodyComent").empty();
                            	$("table#TabPublic").DataTable().ajax.reload();
                                $("#comentario").val('');
                            });
                       
                    }else
                    alert(data);
                }
            })
            .fail(function(){
                swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
            });        }
   		});

	});
  	</script>
  	<script src="../../public/js/publicacionesValidate.js"></script>

</head>
<nav class="navbar navbar-expand-lg navbar-dark bg-grandiant">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse " id="navbarsExample08">
    <ul class="navbar-nav">
       <li class="nav-item ">
        <img width="150px" src="../../public/images/Logo.jpg" alt="">
        <!-- <a class="nav-link float" href="index.php">Academia Virtual<span class="sr-only">(current)</span></a> -->
      </li>
      <li class="nav-item active">
        <!-- <img width="150px" src="../../public/images/Logo.jpg" alt=""> -->
        <a class="nav-link float" href="index.php">Academia Virtual<span class="sr-only">(current)</span></a>
      </li>
    </ul>
  </div>
      <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample10" >

      
    </div>
          <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample10" >
<ul class="navbar-nav">
              <li class="nav-item active">
        <a class="nav-link float" href="http://localhost/AulaVirtual/Views/consulta/">Foro<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link active dropdown-toggle" href="#" id="dropdown08" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Opciones</a>
        <div class="dropdown-menu" aria-labelledby="dropdown08">
          <a class="dropdown-item" href="http://localhost/AulaVirtual/Views/usuarios/miPerfil.php">Perfil</a>
          <a class="dropdown-item" href="http://localhost/AulaVirtual/views/cursos/">Panel de Control</a>
          <a class="dropdown-item" href="http://localhost/AulaVirtual/index.php?logout">Salir</a>
        </div>
      </li>



</ul>
    </div>
</nav>
<body>


<div class="container-fluid">
	<div class="row">
		<div class="col-md-5"></div>
		<div class="col-md-4"><h3>Publicaciones</h3></div>
	</div>
	<div class="row">
		<div class="col-md-9"></div>
		<div class="col-md-1"><button type="button" class="btn btn-primary" data-toggle="modal" data-target=	"#exampleModal">Nueva Publicación</button>
		</div>
	</div>
	<br/><br/>
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<table class="table table-hover table-striped" id="TabPublic">
				<thead>
					<th>N°</th>
					<th>Título</th>
					<th>Descripcion</th>
					<th>N° comentarios</th>
					<th>Fecha</th>
					<th>Acción</th>
				</thead>
				<tbody>
				</tbody>
				<tfoot>
					<th>N°</th>
					<th>Título</th>
					<th>Descripcion</th>
					<th>N° comentarios</th>
					<th>Fecha</th>
					<th>Acción</th>
				</tfoot>
			</table>
		</div>
		<div class="col-md-1"></div>
	</div>
	<!-- modal -->
	<div id="M_comentarios" class="modal fade" role="dialog">
  		<div class="modal-dialog modal-lg">

    		<!-- Modal content-->
    		<div class="modal-content">
      			<div class="modal-header">
              <h4 id="TituloModal" class="modal-title"></h4>
        			<button type="button" class="close" data-dismiss="modal">&times;</button>
        			
      			</div>
      			<div class="modal-body">
      				<div class="row">
						<div class="col-md-1"><button type="button" class="btn btn-info" data-toggle="modal" data-target=	"#ModalComentario">Comentario</button>
						</div>
					</div><br>
					<div class="content-wrapper">
				        <section class="content">
						  <div class="container-fluid">
						    <div class="row">
						      <div class="col-md-12">
						        <div class="card card-primary card-outline">
						          <div class="card-header">
						            <h3 class="card-title">
						              <i class=""></i>
						            </h3>
						          </div>
						          	<div class="card-body">
						              <div class="timeline" id="ModelBodyComent">
									  <!-- timeline time label -->
								      <!-- timeline item -->
									  </div>
									  <!-- END timeline item -->




						          </div>
						          <!-- /.card -->
						        </div>
						      </div>
						    </div>
						  </div>
						</section>
					</div>
      			</div>
      			<div class="modal-footer">
        			<button type="button" class="btn btn-default" data-dismiss="modal" >Cerrar</button>
      			</div>
    		</div>
  		</div>
	</div>


	<!-- Modal -->
	<div class="modal fade bd-example-modal-xl" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  		<div class="modal-dialog modal-xl" role="document">
    		<div class="modal-content">
      			<div class="modal-header">
        			<h5 class="modal-title" id="exampleModalLabel">Nueva Publicación</h5>
        			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          				<span aria-hidden="true">&times;</span>
        			</button>
      			</div>
      			<div class="modal-body">
   
      				<form id="NewPublic" method="POST" action="guardarPublicacion.php">
          				<input type="hidden" name="usuario" value="<?php echo $usuario; ?>">
        				<div class="row">
              				<div class="col-sm-6">
                				<!-- text input -->
                				<div class="form-group">
	                  				<label>Titulo</label>
	                  				<input type="text" name="titulo" id="titulo" required="true" class="form-control" autofocus autocomplete="off">
                				</div>
          					</div>
              				<div class="col-sm-6">
		                		<div class="form-group">
		                  			<label>Decripciom</label>
		                  			<textarea class="form-control" autocomplete="off" required="true" name="descripcion" id="descripcion" value=""> </textarea>
		                		</div>
	              			</div>
            			</div>

            			<div class="row">
              				<div class="col-sm-6">
                				<!-- select -->
                				<div class="form-group">
                  					<label>Curso</label>
                  					<select name="curso" id="curso" class="form-control"> 
                  					</select>
                				</div>
              				</div>
              				<div class="col-sm-6">
                				<div class="form-group">
	                  				<label>Area</label>
	                  				<select name="area" id="area" class="form-control">
	                  				</select>
                				</div>
              				</div>
            			</div>
      			<div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" id="guardar" class="btn btn-primary">Guardar</button>
              
            </div>
				       
    				</form>
        
      			</div>
    		</div>
  		</div>
	</div>
	<!--end modal-->
	<!-- Modal -->
	<div class="modal fade" id="ModalComentario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  		<div class="modal-dialog" role="document">
    		<div class="modal-content">
      			<div class="modal-header">
        			<h5 class="modal-title" id="exampleModalLabel">Nuevo Comentario</h5>
        			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          				<span aria-hidden="true">&times;</span>
        			</button>
      			</div>
      			<div class="modal-body">
   
      				<form id="NewComent" method="POST">
          				<input type="hidden" name="usuario" value="<?php echo $usuario; ?>">
          				<input type="hidden" name="idPublicacion" id="idPublicacion">
        				<div class="row">
              				<div class="col-sm-1"></div>
              				<div class="col-sm-10">
		                		<div class="form-group">
		                  			<label>Comentario</label>
		                  			<textarea class="form-control" required="true" name="comentario" id="comentario"> </textarea>
		                		</div>
	              			</div>
            			</div>
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				        <button type="submit" class="btn btn-primary">Guardar</button>
    				</form>
        
      			</div>
    		</div>
  		</div>
	</div>
	<!--end modal-->
</div>

</body>
</html>
<?php }else{ ?>
<div>
    <h1>
        No Se encontro ninguna session Activa por favor Valla al Login. &nbsp;&nbsp;&nbsp;
        <a href="index.php">Login</a>
    </h1>
</div>
<?php } ?>

