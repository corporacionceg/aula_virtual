 <?php 
 session_start();
if (isset($_GET['curso'])) {
	?>
	<input type="hidden" id="idcurso" value="<?php echo $_GET['curso']; ?>">
	<?php
}
  ?>
   <?php 
if (isset($_GET['ver'])) {
  ?>
  <input type="hidden" id="ver" value="<?php echo $_GET['ver']; ?>">
  <?php
}
  ?>
  <!DOCTYPE html>
<html>
<head>
	<title>Carga de clases</title>
         <script src="../../public/js/popper.min.js"></script>
    <script src="../../public/js/jquery-3.4.1.min.js"></script>
    <script src="../../public/js/bootstrap.min.js"></script>
    <script src="../../public/js/jquery.validate.js"></script>
    <script src="../../public/js/additional-methods.js"></script>
    <script src="../../public/js/sweetalert2.js"></script>
    <script src="../../public/js/jquery.mask.min.js"></script>
    <script src="../../public/js/all.js"></script>
    <script src="../../public/js/jquery.dataTables.min.js"></script>
    <script src="../../public/js/select2.min.js"></script>
    <script src="../../public/js/jquery.form.min.js"></script>
    <script src="../../public/js/java.cargaClases.js"></script>
    

  	<!-- <link rel="stylesheet" href="../../public/css/bootstrap.css"> -->
    <link rel="stylesheet" href="../../public/css/select2.min.css">
    <link rel="stylesheet" href="../../public/css/select2-bootstrap4.css">
    <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../public/css/all.css">
    <link rel="stylesheet" href="../../public/css/sweetalert2.css">
    <link rel="stylesheet" href="../../public/css/default.css">
    <link rel="stylesheet" href="../../public/css/jquery.dataTables.min.css">
     <STYLE type="text/css">
    *{box-shadow: none!important;}
   #fondo{
   	background: rgb(179,238,174);
background: radial-gradient(circle, rgba(179,238,174,1) 0%, rgba(148,187,233,1) 100%);
   }
 </STYLE>
    </head>
<body>
    <?php include("../../config/menu.php"); ?>
    <div class="nuevo container-fluid p-4 border-bottom border-primary">
      <a href="../../views/main/" class="btn btn-primary"> Ir al Inicio</a>
    </div>
<div id="fondo"  class="container-fluid p-4 border-bottom border-primary" >
	
    <div class="container">
      <img src="../../api/renderImagenes.php?id=<?php echo $_GET['curso']; ?>" width="200px" alt="">
      <h1 class="jumbotron-heading" id="Tcurso"></h1>
      <p class="lead text-muted" id="Dcurso"></p>
      <h5 id="idioma"></h5>
      <p>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
 Agregar clase
</button>
        <a href="#" class="btn btn-secondary my-2">Salir</a>
      </p>
      
    </div>
<div class="float-right" id="fecha"></div>
</div>
<div class="container-fluid bg-light">
	<div class="album py-5 ">
	    <div class="container">

		    <div class="row" id="DivClases">

	      </div>
	    </div>
  	</div>



</div>
<!-- Modal -->
<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl ">
    <div class="modal-content" id="DivVideo">

    </div>
  </div>
</div>
<!-- Modal -->


<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Cargar Clase</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="uploadImage" action="../../api/subirVideo.php" method="post" enctype="multipart/form-data">
      <div class="modal-body p-4">

  <div class="row">
      <div class="input-group  mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="labeltitulo">Titulo</span>
            </div>
            <input type="hidden" name="curso" value="<?php echo $_GET['curso']; ?>">
        <input type="text" class="form-control border-top-0 border-right-0" name="titulo" id="titulo"   required>
      </div>
    </div>
     <div class="row">
     <div class="input-group  mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="labeltitulo">Descripcion</span>
            </div>
        <textarea type="text" row="5" class="form-control border-top-0 border-right-0" name="decripcion" id="decripcion" required></textarea>
      </div>
</div>

       <div class="row">
<div class="custom-file">
  <input type="file" class="custom-file-input" name="uploadFile" id="uploadFile" required>
  <label class="custom-file-label" for="uploadFile" >VIDEO DE LA CLASE</label>
</div>
</div>
<br>
       <div class="row">
<div class="custom-file">
  <input type="file" class="custom-file-input" name="uploadFile1" id="uploadFile1" required>
  <label class="custom-file-label" for="uploadFile" >PORTADA DE LA CLASE</label>
</div>
</div>
<br>
     <div class="row">
     <div class="input-group  mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="labeltitulo">actividad</span>
            </div>
        <textarea type="text" row="5" class="form-control border-top-0 border-right-0" name="actividad" id="actividad" required></textarea>
      </div>
</div>
        <div class="progress">
       <div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
      </div>


 <div id="targetLayer" style="display:none;"></div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" id="uploadSubmit" value="Upload" class="btn btn-primary ">Guardar <button/>
      </div>
     </form>

    </div>
  </div>
</div>
<!-- modal -->
  <?php include('../../config/footer.php'); ?>
</body>
</html>