<?php session_start();
if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])){ 
$usuario = $_SESSION['user_id']; 
    
?>
<!DOCTYPE html>
<html>
<head>
	<title>Cursos</title>
         <script src="../../public/js/popper.min.js"></script>
    <script src="../../public/js/jquery-3.4.1.min.js"></script>
    <script src="../../public/js/bootstrap.min.js"></script>
    <script src="../../public/js/jquery.validate.js"></script>
    <script src="../../public/js/additional-methods.js"></script>
    <script src="../../public/js/sweetalert2.js"></script>
    <script src="../../public/js/jquery.mask.min.js"></script>
    
    <script src="../../public/js/all.js"></script>
    <script src="../../public/js/jquery.dataTables.min.js"></script>

  	<!-- <link rel="stylesheet" href="../../public/css/bootstrap.css"> -->
    <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../public/css/all.css">
    <link rel="stylesheet" href="../../public/css/sweetalert2.css">
    <link rel="stylesheet" href="../../public/css/default.css">
    <link rel="stylesheet" href="../../public/css/jquery.dataTables.min.css">
  	<script>
  	$(document).ready(function(){
  		$("table#TabCursos").DataTable({
			"language": {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
			  "sFirst":    "Primero",
			  "sLast":     "Último",
			  "sNext":     "Siguiente",
			  "sPrevious": "Anterior"
			},
			  "oAria": {
			      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			  }
			},
      "destroy":true,
      "responsive":true,
      "paging":true,
      "autoWidth":false,
			"ajax":{
			  "type":"POST",
			  /*"data":{"opcion" :1},*/
			  "url":"../../api/colsultarCursos.php"
			},
			"columns":[
			  {"data":"id_curso"},
			  {"data":"titulo"},
			  {"data":"descripcion"},
			  {"data":"idioma"},
			  {"data":"fechaC"},
			  {"data":"nombre"},
			  {"defaultContent":"<button id='NClases'  class='btn btn-info glyphicon glyphicon-pencil'> Ver Curso</button>&nbsp;<button id='borrar'  class='btn btn-danger glyphicon glyphicon-remove'>Eliminar</button>"},
			],
		});

		var tabla =$("table#TabCursos").DataTable();
    	$("table#TabCursos tbody").on("click","button#NClases",function(){
    		var fila = tabla.row($(this).parents("tr")).data();
    		var idCurso=fila.id_curso;
    		// alert(idCurso);
        window.location.replace("cargaClases.php?curso="+idCurso+"&ver=1");
    	});

      $("table#TabCursos tbody").on("click","button#borrar",function(){
        var fila = tabla.row($(this).parents("tr")).data();
        var idCurso=fila.id_curso;
        // alert(idCurso);
            Swal.fire({
  title: 'Estas seguro que decea eliminar el Curso?',
  text: "Se borraran todos los registros relacionados!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, Borrar!'
}).then((result) => {
    if (result.value) {
$.ajax({
          url: '../../api/borrarcurso.php',
          type: 'POST',
          dataType: 'json',
          data: {id:idCurso},
    success:function(data){
      if (data == 1) {
      Swal.fire({
  type: 'success',
  title: 'Eliminado con exito...',
  text: 'El Curso fue Eliminado!',
});
      }else{
              Swal.fire({
  icon: 'error',
  title: 'El Curso no se puede eliminar...',
  text: 'Ya este Curso fue realizado por un usuario',
});
      }

  
    }
  });
}


  
});
          
});



  	});
  </script>
</head>
<body>
	<?php include("../../config/menu.php"); ?>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-1-"></div>
        <div class="col-md-1-">
          <a href="../../views/main/" class="btn btn-primary"> Ir al Inicio</a>
        </div>      
      </div>

      <div class="card m-4">
        <div class="card-header">
      <h1 class="center">Cursos</h1>
          
        </div>
        <div class="row m-4" >
    
      <div class="col"><a type="button" href="registrarCurso.php" class="btn btn-primary" >Nuevo Curso</a>
      </div><br><br>
    </div>  
        <div class="row m-4 ">
          <div class="col">
                    <table class="table table-hover table-striped" id="TabCursos">
                <thead>
                    <th>N°</th>
                    <th>Título</th>
                    <th>Descripción</th>
                    <th>Idioma</th>
                    <th>Fecha de Creación</th>
                    <th>Area</th>
                    <th>Acción</th>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <th>N°</th>
                    <th>Título</th>
                    <th>Descripción</th>
                    <th>Idioma</th>
                    <th>Fecha de Creación</th>
                    <th>Area</th>
                    <th>Acción</th>
                </tfoot>
            </table>
          </div>

          
        </div>
    </div>
</div>

  <?php include('../../config/footer.php'); ?>
</body>
</html>
<?php }else{ ?>
<div>
    <h1>
        No Se encontro ninguna session Activa por favor Valla al Login. &nbsp;&nbsp;&nbsp;
        <a href="index.php">Login</a>
    </h1>
</div>
<?php } ?>