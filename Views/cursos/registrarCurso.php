<?php
session_start();
 if (isset($_GET['response'])) {
        if ($_GET['response'] == 1) {
            ?><input type="hidden" id="error" value="<?php echo $_GET['response']; ?>"><?php
        }
} 
?>
<!DOCTYPE html>
<html>
<head>
	<title>Cursos</title>
    <script src="../../public/js/jquery-3.4.1.min.js"></script>
    <script src="../../public/js/bootstrap.min.js"></script>
    <script src="../../public/js/jquery.validate.js"></script>
    <script src="../../public/js/additional-methods.js"></script>
    <script src="../../public/js/sweetalert2.js"></script>
    <script src="../../public/js/jquery.mask.min.js"></script>
         <script src="../../public/js/popper.min.js"></script>
    
    <script src="../../public/js/all.js"></script>
    <script src="../../public/js/jquery.dataTables.min.js"></script>
    <script src="../../public/js/select2.min.js"></script>
    <script src="../../public/js/java.cargaCurso.js"></script>

  	<!-- <link rel="stylesheet" href="../../public/css/bootstrap.css"> -->
    <link rel="stylesheet" href="../../public/css/select2.min.css">
    <link rel="stylesheet" href="../../public/css/select2-bootstrap4.css">
    <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../public/css/all.css">
    <link rel="stylesheet" href="../../public/css/sweetalert2.css">
    <link rel="stylesheet" href="../../public/css/default.css">
    <link rel="stylesheet" href="../../public/css/jquery.dataTables.min.css">
     <STYLE type="text/css">
    *{box-shadow: none!important;}
 </STYLE>
    </head>
<body>
    <?php include("../../config/menu.php"); ?>
    <div class="container-fluid">
    <form action="../../api/registrarCurso.php" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
<div class="container mt-4 ">
<!-- TITULO -->
<div class="card shadow  mb-5 bg-white rounded" id="divTitulo">
    <div class="card-header">
        TITULO DEL CURSO
    </div>
    <div class="card-body">
        <div class="alert alert-danger" id="alertTitulo" role="alert" style="display:none">
            El titulo no puede estar vacio.
        </div>
       <div class="input-group  mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="labeltitulo">Titulo</span>
            </div>
        <input type="text" class="form-control border-top-0 border-right-0" name="titulo" id="titulo"  >
        </div>
    </div>
    <div class="card-footer">
        <a type="button" href="#divDescrip" id="btitulo" onclick="mostrarDes();" class="btn btn-outline-primary float-right">Continuar</a>
    </div>
</div>
<!-- DESCRIPCION -->
<div class="card shadow  mb-5 bg-white rounded" style="display: none;" id="divDescrip">
    <div class="card-header">
        DESCRIPCIÓN DEL CURSO
    </div>
    <div class="card-body">
  <div class="input-group">
                <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-default">Descripción</span>
            </div>
    <textarea class="form-control border-top-0 border-right-0" name="descripcion" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>
    </div>
    <div class="card-footer">
        <a type="button" href="#divArea" id="bdescrip" onclick="mostrarArea();" class="btn btn-outline-primary float-right">Continuar</a>
    </div>
</div>
    <!-- AREA -->
<div class="card shadow  mb-5 bg-white rounded" style="display: none;" id="divArea">
    <div class="card-header">
       AREA DEL CURSO
    </div>
    <div class="card-body">
        <div class="alert alert-danger" id="alertArea" role="alert" style="display:none">
            El Area no puede estar vacia.
        </div>
<div class="input-group mb-3">
  <div class="input-group-prepend">
    <label class="input-group-text" for="selectArea">Area</label>
  </div>
  <select class="form-control border-top-0 border-right-0" name="area" id="selectArea">

  </select>
</div>
</div>
    <div class="card-footer">
        <a type="button" href="#divFoto" id="barea" onclick="mostrarFotos();" class="btn btn-outline-primary float-right">Continuar</a>
    </div>
</div>

<!-- CARGAR FOTO -->
<div class="card shadow   mb-5 bg-white rounded" style="display: none;" id="divFoto">
    <div class="card-header">
       CARGAR FOTO DE PORTADA
    </div>
    <div class="card-body " style="padding-left:200px;padding-right: 200px;">

<div class="text-center" >
    <div>Click en la imagen para subir el archivo.</div>
<div class="image-upload">
    <label for="foto">
         <img src="../../public/images/foto.svg" id="blah" alt="SubirFoto" alt ="Click aquí para subir tu foto" title ="Click aquí para subir tu foto">
    </label>
        <input id="foto" name="foto" type="file"/>
</div>
</div>

</div>
    <div class="card-footer">
        <a type="button" href="#divDatos" id="bfoto" onclick="mostrarDatos();" class="btn btn-outline-primary float-right">Continuar</a>
    </div>
</div>
<!-- DATOS -->
<div class="card shadow   mb-5 bg-white rounded" style="display: none;" id="divDatos">
    <div class="card-header">
       OTROS DATOS
    </div>
<div class="card-body " >

  <div class="row">
    <div class="col">
         <label class="sr-only" for="idioma">Subtitulos</label>
      <div class="input-group mb-2">
        <div class="input-group-prepend">
          <div class="input-group-text">Idioma</div>
        </div>
       <input type="text" id="idioma" name="idioma" class="form-control border-top-0 border-right-0" placeholder="IDIOMA" required>
      </div>
      
    </div>
    <div class="col">
     <label class="sr-only" for="subtitulo">Subtitulos</label>
      <div class="input-group mb-2">
        <div class="input-group-prepend">
          <div class="input-group-text">Subtitulos</div>
        </div>
        <input type="text" id="subtitulo" name="subtitulo" class="form-control border-top-0 border-right-0" placeholder="SUBTITULOS">
      </div>
      
    </div>
        <div class="col">
     <label class="sr-only" for="Privacidad">Privacidad</label>
      <div class="input-group mb-2">
        <div class="input-group-prepend">
          <div class="input-group-text">Privacidad</div>
        </div>
        <select class="custom-select" id="Privacidad" name="Privacidad" required>
        <option value=""></option>
        <option value="0">Publico</option>
        <option value="1">Privado</option>
        </select>
      </div>
      
    </div>
  </div>

</div>
<div class="card-footer">
        <button type="submit" class="btn btn-outline-primary float-right">Guardar</button>
</div>
</div>



</div>
    </form>





    </div>

  <?php include('../../config/footer.php'); ?>
</body>
</html>