<?php session_start();
if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])){ 
$usuario = $_SESSION['user_id']; 
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Academia Virtual</title>
    <script src="../../public/js/popper.min.js"></script>
    <script src="../../public/js/jquery-3.4.1.min.js"></script>
    <script src="../../public/js/bootstrap.min.js"></script>
    <script src="../../public/js/jquery.validate.js"></script>
    <script src="../../public/js/additional-methods.js"></script>
    <script src="../../public/js/sweetalert2.js"></script>
    <script src="../../public/js/jquery.mask.min.js"></script>
    
    <script src="../../public/js/all.js"></script>
    <script src="../../public/js/jquery.dataTables.min.js"></script>
     
    <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../public/css/all.css">
    <link rel="stylesheet" href="../../public/css/sweetalert2.css">
    <link rel="stylesheet" href="../../public/css/default.css">
    <link rel="stylesheet" href="../../public/css/jquery.dataTables.min.css">

<script src="../../public/js/java.js"></script>
</head>

<body>
   <?php include("../../config/menu.php");?>
    
<div  class="container-fluid  ">
    <div class="row p-2 ">
<!-- /FORMULARIO DE REGISTRO -->
      <div class="col" >
<div class="card text-center">
  <div class="card-header">
    Registrar usuario 
  </div>
  <form id="NewUsers" method="POST">
  <div class="card-body">
            <div class="row">
          <!-- <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
          <div class="col">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Registro de usuarios!</h1>
              </div>
            
                 <div class="form-group">
                  <div  id="labelusuario" >
                    
                  </div>
                  <input type="text" onkeyup="ValidarUsuario();" class="form-control form-control-user" id="usuario" name="usuario"  placeholder="Usuario">
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" id="nombre" name="nombre" placeholder="Nombre">
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-user" id="apellido" name="apellido" placeholder="Apellido">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="Contraseña">
                  </div>
                  <div class="col-sm-6">
                    <input type="password" class="form-control form-control-user" id="passwordRepeat" name="passwordRepeat" placeholder="Repetir contraseña">
                  </div>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control form-control-user" id="correo" name="correo" placeholder="Correo">
                </div>
             <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="tel" class="form-control form-control-user tlf"  placeholder="Telefono" name="tlf" maxlength="15">
                  </div>
            </div>

               
          
            </div>
          </div>
        </div>
  </div>
  <div class="card-footer text-muted">
     <button type="submit"  class="btn btn-primary btn-user btn-block">Guardar</button> 
  </div>
  </form>
</div>
      </div>

<!-- /FORMULARIO DE REGISTRO -->
<!-- LISTA DE USUARIOS -->
      <div class="col-sm-7 ">
        <div class="card text-center">
  <div class="card-header">
    Lista de usuarios 
  </div>
          <div class="card-body">
           
          <table width="100%" class="table table-sm table-striped table-bordered" id="tabUser">
              <thead>
    <tr>
      <!-- <th scope="col">Nick</th> -->
      <th scope="col">Nombre y Apellido</th>
      <th scope="col">Correo</th>
      <th scope="col">Tlf</th>
      <th scope="col">Accion</th>
    </tr>
  </thead>
  <tbody>
  </tbody>



          </table>
          

  





          </div>
        </div>
      </div>

<!-- /LISTA DE USUARIOS -->






    </div>
        
</div>


  <!-- Modal -->
  <div class="modal fade bd-example-modal-xl" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel"></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="editUser">
              <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Datos de Usuario</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">

                <div class="row">
                      <div class="col-sm-6">
                        <!-- text input -->
                        <div class="form-group">
                            <label>Usuario</label>
                            <input type="text" name="usuario2" id="usuario2" readonly="readonly" required="true" class="form-control" autofocus autocomplete="off">
                        </div>
                    </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label>Telefono</label>
                            <input type="text" name="telefono" id="telefono2" readonly="readonly" required="true" class="form-control tlf" autofocus autocomplete="off">
                        </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-6">
                        <!-- select -->
                        <div class="form-group">
                            <label>Correo</label>
                            <input type="text" name="correo2" id="correo2" readonly="readonly" required="true" class="form-control" autofocus autocomplete="off">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label>Tipo de Usuario</label>
                            <select name="tipo" id="tipo" class="form-control">
                              <option>Selecione</option>
                              <option value="1">Estudiante</option>
                              <option value="2">Profesor</option>
                              <option value="3">Admin</option>
                            </select>
                        </div>
                      </div>
                  </div>

                <div class="row">
                  
                  <div class="col-sm-1">
                    <input type="submit" name="Guardar" value="Guardar" class="btn btn-primary">
                  </div>
                </div>                
              </div>
              <!-- /.card-body -->
            </div>
               
              </form>
        
            </div>
        </div>
      </div>
  </div>
  <!--end modal-->
    

    <?php include("../../config/footer.php");?>



</body>
</html>
<?php }else{ ?>
<div>
    <h1>
        No Se encontro ninguna session Activa por favor Valla al Login. &nbsp;&nbsp;&nbsp;
        <a href="index.php">Login</a>
    </h1>
</div>
<?php } ?>