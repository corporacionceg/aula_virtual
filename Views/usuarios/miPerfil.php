<?php session_start();
if (isset($_SESSION['user_id'])){ 
$usuario = $_SESSION['user_id']; 
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi Perfil</title>
    <script src="../../public/js/jquery-3.4.1.min.js"></script>
    <!-- <script src="../../public/js/jquery-3.3.1.slim.min.js"></script> -->
    <script src="../../public/js/jquery.validate.js"></script>
    <script src="../../public/js/bootstrap.min.js"></script>
    <script src="../../public/js/adminlte.js"></script>
     <script src="../../public/js/sweetalert2.js"></script>
    <script src="../../public/js/all.js"></script>
      <script src="http://malsup.github.com/jquery.form.js"></script> 
    <script src="../../public/js/popper.min.js"></script>
    
     
    <link rel="stylesheet" href="../../public/css/adminlte.css">
    <link rel="stylesheet" href="../../public/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../public/css/all.css">
    <link rel="stylesheet" href="../../public/css/sweetalert2.css">
    <link rel="stylesheet" href="../../public/css/default.css">
    <link rel="stylesheet" href="../main/album.css">
    <script type="text/javascript">
      $(document).ready(function(){

$('#uploadImage').submit(function(event){
  if($('#uploadFile').val())
  {
   event.preventDefault();
   $('#loader-icon').show();
   $('#targetLayer').hide();
   $(this).ajaxSubmit({
    target: '#targetLayer',
    beforeSubmit:function(){
     $('.progress-bar').width('50%');
    },
    uploadProgress: function(event, position, total, percentageComplete)
    {
     $('.progress-bar').animate({
      width: percentageComplete + '%'
     }, {
      duration: 100
     });
      $('.progress-bar').html(percentageComplete + '%');
    },
  success:function(){
       $('.progress-bar').width('100%');
       $('.progress-bar').addClass('bg-success');
        Swal.fire(
  'Tu clase se ha registro con exito! ',
  '',
  'success'
);
        location.reload();
    },
    resetForm: true,

   });

  }
  return false;
 });








        $.getJSON("../../views/usuarios/consultarUsuario.php",function(datos){
            if(datos != 0){              
                $.each(datos,function(K,V){
                  var nombre = V['nombre']+' '+V['apellido'];
                  var mayus=  nombre.toUpperCase();
                    
                    $("#NombreUser").append('<b>'+mayus+'</b>');
                    $("li#user").append('<b>Usuario</b>: '+V['usuario']);
                    $("#usuario").val(V['usuario']);
                    $("#InputUsuario").val(V['usuario']);

                    $("li#direccion").append('<b>Direccion</b>: '+V['direccion']);
                    $("textarea#direc").val(V['direccion']);

                    $("li#telefono").append('<b>Tel&eacute;fono #</b>: '+V['tlf']);
                    $("#telef").val(V['tlf']);

                    $("li#correo").append('<b>Correo</b>: '+V['correo']);
                    $("#corre").val(V['correo']);

                    $("li#descripc").append('<b>Descripci&oacute;n</b> : '+V['descripcion']);
                    $("#descripcion").val(V['descripcion']);

                    var cv=V['curriculo'];
                    
                    if (cv.length > 2) {
                      var btn='<a class="btn btn-xs btn-success" href="download.php?file='+cv+'" ><i class="fas fa-id-card"></i>&nbsp;Descargar CV</a>'
                      $("#divDescarga").append(btn);
                    }
                });
            }
        });
            //,
        $("form#EditarPerfil").validate({
          rules : {
              telefono : {
                  required : true,
                  minlength:11,
                  maxlength:16
              },
              corre : {
                  required : true,
                  minlength:8,
                  maxlength:50
              },
              usuario : {
                  required : true,
              },
              descripcion : {
                  required : true,
                  minlength:10,
                  maxlength:250
              },
              direccion : {
                  required : true,
                  minlength:10,
                  maxlength:250
              },
          },
          messages : {
              telefono : {
                  required : "El Telefono es Requerido",
                  minlength:"La Cantidad Mínima es de Once (11) Caracteres.",
                  maxlength:"La Cantidad Máxima es de dieciseis (16) Caracteres.",
              },
              corre : {
                  required : "El Correo es Requerido",
                  minlength:"La Cantidad Mínima es de Ocho (8) Caracteres.",
                  maxlength:"La Cantidad Máxima es de Cincuenta (50) Caracteres.",
              },
              usuario : {
                  required : "El Usuario es Requerido",
              },
              descripcion : {
                  required : "La Descripcion es Requerido",
                  minlength:"La Cantidad Mínima es de Diez (10) Caracteres.",
                  maxlength:"La Cantidad Máxima es de Docientos Cincuenta (250) Caracteres."
              },
              direccion : {
                  required : "La Direccion es Requerido",
                  minlength:"La Cantidad Mínima es de Diez (10) Caracteres.",
                  maxlength:"La Cantidad Máxima es de Docientos Cincuenta (250) Caracteres."
                  
              },
          },
          submitHandler:function(){
              parametros = $("form#EditarPerfil").serialize();
              $.ajax({
                  url : "actualizarUsuario.php",
                  type : "POST",
                  data : parametros,
                  success: function(data){
                      console.log(data);
                      if(data==1){
                          //alert(data)
                           //transaccion exitosa
                              _Title = "¡Enhorabuena!";
                              _Text = "Transacción exitosa";
                              _Type = "success";

                            Swal.fire({
                                  text : _Text,
                                  title: _Title,
                                  timer: 3000,
                                  type : _Type,

                                  onBeforeOpen: function (){
                                      swal.showLoading()
                                  }
                              }).then((result)=>{
                                  var url = "http://localhost/AulaVirtual/Views/usuarios/miPerfil.php"; 
                                  window.location.replace(url);   
                              });
                         
                      }else
                      alert(data);
                  }
              })
              .fail(function(){
                  swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
              });        
            }
        });

        // $("form#uploadImage").validate({
        //   rules : {
        //       direccion : {
        //           required : true,
        //       },
        //   },
        //   messages : {
              
        //       direccion : {
        //           required : "La Direccion es Requerido",
        //       },
        //   },
        //   submitHandler:function(){
        //       parametros = $("form#uploadImage").serialize();
        //       $.ajax({
        //           url : "guardarCurriculo.php",
        //           type : "POST",
        //           data : parametros,
        //           success: function(data){
        //               console.log(data);
        //               if(data==1){
        //                   //alert(data)
        //                    //transaccion exitosa
        //                       _Title = "¡Enhorabuena!";
        //                       _Text = "Tu Curriculo se ha registro con exito!";
        //                       _Type = "success";

        //                     Swal.fire({
        //                           text : _Text,
        //                           title: _Title,
        //                           timer: 3000,
        //                           type : _Type,

        //                           onBeforeOpen: function (){
        //                               swal.showLoading()
        //                           }
        //                       }).then((result)=>{
        //                           var url = "http://localhost/AulaVirtual/Views/usuarios/miPerfil.php"; 
        //                           window.location.replace(url);   
        //                       });
                         
        //               }else
        //               alert(data);
        //           },
        //           beforeSubmit:function(){
        //             $('.progress-bar').width('50%');
        //           },
        //           uploadProgress: function(event, position, total, percentageComplete){
        //             $('.progress-bar').animate({
        //               width: percentageComplete + '%'
        //             }, {
        //               duration: 100
        //             });
        //               $('.progress-bar').html(percentageComplete + '%');
        //           }
        //       })
        //       .fail(function(){
        //           swal("FATAL-ERROR"," ERROR DE AJAX :( :( ","error");
        //       });        
        //     }
        // });

        // $('#uploadImage').submit(function(event){
        //   event.preventDefault();
        //   $(this).ajaxSubmit({
        //     target: '#targetLayer',
        //     beforeSubmit:function(){
        //      $('.progress-bar').width('50%');
        //     },
        //     uploadProgress: function(event, position, total, percentageComplete)
        //     {
        //      $('.progress-bar').animate({
        //       width: percentageComplete + '%'
        //      }, {
        //       duration: 100
        //      });
        //       $('.progress-bar').html(percentageComplete + '%');
        //     },
        //     success:function(data){
        //       if(data==1){

        //        $('.progress-bar').width('100%');
        //        $('.progress-bar').addClass('bg-success');
        //         Swal.fire(
        //           'Tu Curriculo se ha registro con exito! ',
        //           '',
        //           'success'
        //         );
        //       }else{
        //         alert(data);
        //       }
        //     },
        //     resetForm: true,

        //   });

        // });





      });
    </script>

</head>
<nav class="navbar navbar-expand-lg navbar-dark bg-grandiant">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse " id="navbarsExample08">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link float" href="http://localhost/AulaVirtual/Views/main/">Academia Virtual<span class="sr-only">(current)</span></a>
      </li>

    </ul>
  </div>
      <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample10" >

      
    </div>
          <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample10" >
<ul class="navbar-nav">
              <li class="nav-item active">
        <a class="nav-link float" href="http://localhost/AulaVirtual/Views/consulta/">Foro<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link active dropdown-toggle" href="#" id="dropdown08" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Opciones</a>
        <div class="dropdown-menu" aria-labelledby="dropdown08">
          <a class="dropdown-item" href="http://localhost/AulaVirtual/Views/usuarios/miPerfil.php">Perfil</a>
          <a class="dropdown-item" href="http://localhost/AulaVirtual/views/cursos/">Panel de Control</a>
          <a class="dropdown-item" href="http://localhost/AulaVirtual/index.php?logout">Salir</a>
        </div>
      </li>



</ul>
    </div>
</nav>
<body>
 
   <section class="content">
      <div class="container-fluid">
        <br>
        
        <div class="row">
          <div class="col-1"></div>
          <div class="col-10">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Mi Perfil</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-4"><div id="divDescarga"></div></div>
                  <div class="col-md-4"></div>
                  <div class="col-md-4">
                    <div class="btn-group">
                      <button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target=  "#exampleModal"><i class="fas fa-user"></i>&nbsp;Editar Perfil</button>
                      <button type="button" class="btn btn-xs btn-success"data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-id-card"></i>&nbsp;Experiencia (CV)</button>
                              
                    </div>
                  </div>
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-7">
                      <h1 class="lead" id="NombreUser"></h1>
                      <div class="row">
                        <div class="col-md-1"></div>
                        
                        <ul class="ml-4 mb-0 fa-ul text-muted"><br>
                          <li class="small" id="user"><span class="fa-li"><i class="fas fa-user"></i></span> </li><br>
                          <li class="small" id="direccion"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span></li><br>
                          <li class="small" id="telefono"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span></li><br>
                          <li class="small" id="correo"><span class="fa-li"><i class="fas fa-lg fa-envelope-square"></i></span></li><br>
                          <li class="small" id="descripc"><span class="fa-li"><i class="fa fa-list"></i></span></li><br>
                        </ul>
                      </div>


                    </div>
                    <div class="col-5 text-center">
                      <img src="../../public/css/images/usuario.jpeg" alt="" class="img-circle img-fluid">
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
      </div>
    </section>

    <!-- modal -->
    <div class="modal fade bd-example-modal-xl" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Mis Datos</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
   
              <form id="EditarPerfil" method="POST" action="guardarPublicacion.php">
                  <input type="hidden" name="usuario" id="usuario">
                <div class="row">
                      <div class="col-sm-6">
                        <!-- text input -->
                        <div class="form-group">
                            <label>Tel&eacute;fono</label>
                            <input type="text" name="telefono" id="telef" required="true" class="form-control" autofocus autocomplete="off">
                        </div>
                    </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label>Direcci&oacute;n</label>
                            <textarea class="form-control" autocomplete="off" required="true" name="direccion" id="direc" value=""> </textarea>
                        </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-6">
                        <!-- select -->
                        <div class="form-group">
                            <label>Correo</label>
                            <input type="text" name="corre" id="corre" required="true" class="form-control" autofocus autocomplete="off">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label>Descripci&oacute;n</label>
                            <textarea class="form-control" autocomplete="off" required="true" name="descripcion" id="descripcion" value=""> </textarea>
                        </div>
                      </div>
                  </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" id="guardar" class="btn btn-primary">Guardar</button>
              
            </div>
               
            </form>
        
            </div>
        </div>
      </div>
  </div>
  <!-- end modal -->

  <!-- Modal -->
    <div class="modal fade bd-example-modal-xl" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
       <div class="modal-dialog modal-xl" role="document">
          <div class="modal-content">
             <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Sintesis Curricular</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
             </div>
             <form id="uploadImage" action="guardarCurriculo.php" method="post" enctype="multipart/form-data">
              <input type="hidden" name="usuario" id="InputUsuario">
                <div class="modal-body p-4">
                   <div class="row">
                      <div class="custom-file">
                         <input type="file" class="custom-file-input" name="uploadFile" id="uploadFile" required>
                         <label class="custom-file-label" for="uploadFile" >SINTESIS CURRICULAR</label>
                      </div>
                   </div>
                   <br>
                   
                   <div class="progress">
                      <div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                   </div>
                   <div id="targetLayer" style="display:none;"></div>
                </div>
                <div class="modal-footer">
                   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                   <button type="submit" id="uploadSubmit" value="Upload" class="btn btn-primary ">Guardar <button/>
                </div>
             </form>
          </div>
       </div>
    </div>

</body>
</html>

<?php }else{ ?>
<div>
    <h1>
        No Se encontro ninguna session Activa por favor Valla al Login. &nbsp;&nbsp;&nbsp;
        <a href="/AulaVirtual/index.php">Login</a>
    </h1>
</div>
<?php } ?>