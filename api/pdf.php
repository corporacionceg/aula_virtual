<?php
require('../fpdf/fpdf.php');
header('Content-Type: text/html; charset=UTF-8');
class PDF extends FPDF
{
	// Cabecera de página
	function Header()
	{
		// Logo
		$this->Image('../public/images/headerpdf0.png',90,0,200);
		$this->Image('../public/images/headerpdf1.png',100,20,100);
		// Arial bold 15
		$this->SetFont('Arial','B',15);
		// Movernos a la derecha
		$this->Cell(80);
		// Título
		// $this->Cell(30,10,'Title',1,0,'C');
		// Salto de línea
		$this->Ln(20);
	}

	// Pie de página
	function Footer()
	{
		// Posición: a 1,5 cm del final
		$this->SetY(-15);
		// Arial italic 8
		// $this->SetFont('Arial','I',8);
		// Número de página 
		// $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		// $this->Image('../../public/images/headerpdf0.png',90,0,200);
		// $this->Image('../public/images/footerpdf.png',30,150,250);
	}
}
if (isset($_REQUEST['r'])) {
	$curso=$_REQUEST['r'];
	require_once('../config/conexion.php');
	session_start();
	$fecha = date('d-m-Y');
	$usuario = $_SESSION['user_id'];
	$model = new conexion();
	$model->sql="SELECT * FROM usuarios where id_usuario = '$usuario'";
	$Result=$model->EJECUTAR();
	$row = mysqli_fetch_assoc($Result);
	$nombre = $row['nombre'].' '. $row['apellido'];

	$model = new conexion();
	$model->sql="SELECT titulo FROM cursos where id_curso = '$curso'";
	$Result=$model->EJECUTAR();
	$row = mysqli_fetch_assoc($Result);
    $nombrecurso = $row['titulo'];
	$string=" Por haber participado ";
	$string1="en el curso";

	// Creación del objeto de la clase heredada
	$pdf = new PDF();
	$pdf->AliasNbPages();
	$pdf->AddPage('L');
	$pdf->SetFont('Arial','',30);
	// for($i=1;$i<=40;$i++)
	$pdf->SetY(80);
	$pdf->SetX(75);
	$pdf->Cell(150,10,$nombre,0,0,'C');
	//$pdf->ln();
	$pdf->SetFont('Arial','',25);
$pdf->SetY(90);
$pdf->SetX(75);  
$pdf->SetDrawColor(255,155,67);    
$pdf->Line(70 ,90,240, 90);   
	$pdf->Cell(150,10,$string,0,0,'C');
	$pdf->SetY(100);
	$pdf->SetX(75); 
	$pdf->Cell(150,10,$string1,0,0,'C');
	$pdf->SetY(110);
	$pdf->SetX(75);  
	$pdf->Cell(150,10,$nombrecurso,0,0,'C');
	//$pdf->SetLineWidth();

	$pdf->SetY(90);
	$pdf->SetX(75);
	// $pdf->Cell(150,10,$nombre,'B',0,'C');


	$pdf->Image('../public/images/footerpdf.png',30,150,250);
	$pdf->SetFont('Arial','',14);
	$pdf->SetY(160);
	$pdf->SetX(82);
	$pdf->Cell(250,10,$fecha,0,0,'C'); 
	$pdf->Output();
}

?>