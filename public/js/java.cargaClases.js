$(document).ready(function() {

DetallesDeCurso();
MostrarClases();


$('#uploadImage').submit(function(event){
  if($('#uploadFile').val())
  {
   event.preventDefault();
   $('#loader-icon').show();
   $('#targetLayer').hide();
   $(this).ajaxSubmit({
    target: '#targetLayer',
    beforeSubmit:function(){
     $('.progress-bar').width('50%');
    },
    uploadProgress: function(event, position, total, percentageComplete)
    {
     $('.progress-bar').animate({
      width: percentageComplete + '%'
     }, {
      duration: 100
     });
      $('.progress-bar').html(percentageComplete + '%');
    },
  success:function(){
       $('.progress-bar').width('100%');
       $('.progress-bar').addClass('bg-success');
        Swal.fire(
  'Tu clase se ha registro con exito! ',
  '',
  'success'
);
        MostrarClases();

    },
    resetForm: true,

   });

  }
  return false;
 });


if ($('#ver').val() == 2 ) {  
Swal.fire(
  'Tu Curso se registro con exito! ',
  'Siguiente Paso cargar las clases',
  'success'
)
}});


function DetallesDeCurso(){
var id = $('#idcurso').val();
   $.ajax({
      url: '../../api/DetalleDeCurso.php',
      type: 'POST',
      data: {id:id},
      dataType: 'JSON',
      success: function(data) {
         $('#Tcurso').html(data.titulo);
         $('#Dcurso').html(data.descripcion);
        $('#fecha').html('Fecha: '+data.fechaC);
        $('#idioma').html('Idioma: '+data.idioma);
        $('#inscritos').html('Inscritos: '+data.inscritos);
         // $('#Dcurso').html(data.mensaje);
       }
   })
   .done(function() {
      console.log("success");
   })
   .fail(function() {
      console.log("error");
   })
   .always(function() {
      console.log("complete");
   });


}


function MostrarClases(){
var idcurso = $('#idcurso').val();

$.ajax({
  url: '../../api/mostrarClases.php',
  type: 'POST',
  dataType: 'HTML',
  data: {idcurso: idcurso},
  success: function(data) {
      $('#DivClases').html(data);
  }
})
.done(function() {
  console.log("success");
})
.fail(function() {
  console.log("error");
})
.always(function() {
  console.log("complete");
});
}
function verVideo(id){

$.ajax({
  url: '../../api/verVideo.php',
  type: 'POST',
  
  data: {id: id},
    success: function(data) {
      $('#DivVideo').html(data);
  }
})
.done(function() {
  console.log("success");
})
.fail(function() {
  console.log("error");
})
.always(function() {
  console.log("complete");
});


}

function check(id){


var Label = $('#customSwitch1').prop("checked");
if (Label == true ) {

$.ajax({
  url: '../../api/visto.php',
  type: 'POST',
  dataType: 'JSON',
  data: {id:id,visto:1},
}).done(function() {

$('#actividades').show();

})
.fail(function() {
  console.log("error");
})
.always(function() {
  console.log("complete");
});



}else{

$.ajax({
  url: '../../api/visto.php',
  type: 'POST',
  dataType: 'JSON',
  data: {id:id,visto:0},
}).done(function() {
  console.log("success");
})
.fail(function() {
  console.log("error");
})
.always(function() {
  console.log("complete");
});

}

}