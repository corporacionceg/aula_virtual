$(document).ready(function() {




  $('#btitulo').click(function (event){
event.stopPropagation();
var Position = jQuery('[id="divDescrip"]').offset().top;
jQuery('html, body').animate({ scrollTop: Position }, 1100);
return false;
});
  $('#bdescrip').click(function (event){
event.stopPropagation();
var Position = jQuery('[id="divArea"]').offset().top;
jQuery('html, body').animate({ scrollTop: Position }, 1100);
return false;
});
  $('#barea').click(function (event){
event.stopPropagation();
var Position = jQuery('[id="divFoto"]').offset().top;
jQuery('html, body').animate({ scrollTop: Position }, 1100);
return false;
});
  $('#bfoto').click(function (event){
event.stopPropagation();
var Position = jQuery('[id="divDatos"]').offset().top;
jQuery('html, body').animate({ scrollTop: Position }, 1100);
return false;
});

    $('#selectArea').select2({
        placeholder: 'Selecciona una Area',
        theme: 'bootstrap4',
           ajax: {
          url: '../../api/consultaAreas.php',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      });


  $("#foto").change(function () {
    // Código a ejecutar cuando se detecta un cambio de archivO
    readImage(this);
  });


if ($('#error').val()==1 || $('#error').val()==2) {
  var error = $('#error').val();
  switch(error) {
  case '1':
    Swal.fire({
  type: 'error',
  title: 'Oops...',
  text: 'Ya existe un curso con ese titulo !',
})
    break;
  case '2':
   Swal.fire({
  type: 'error',
  title: 'Oops...',
  text: 'Error Registrando el curso!',
})
    break;
  default:
    // code block
} 



};

});
function readImage (input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          $('#blah').attr('src', e.target.result); // Renderizamos la imagen
      }
      reader.readAsDataURL(input.files[0]);
    }
  };

function mostrarDes(){

var campo = $('#titulo').val();

if (campo != '') {

$('#divTitulo').fadeTo('slow',.6);
$('#divTitulo').append('<div style="position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;opacity:0.4;filter: alpha(opacity = 50)"></div>');

$('#divDescrip').fadeTo('slow',1);
 $('#alertTitulo').hide('slow');
}else{

  $('#alertTitulo').show('slow');
}

}
function mostrarArea(){

$('#divDescrip').fadeTo('slow',.6);
$('#divDescrip').append('<div style="position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;opacity:0.4;filter: alpha(opacity = 50)"></div>');

$('#divArea').fadeTo('slow',1);

}
function mostrarFotos(){
var campo  = $('#selectArea').val();

if (campo != null) {
$('#divArea').fadeTo('slow',.6);
$('#divArea').append('<div style="position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;opacity:0.4;filter: alpha(opacity = 50)"></div>');
$('#divFoto').fadeTo('slow',1);
$('#alertArea').hide('slow');
}else{
$('#alertArea').show('slow');
}

}
function mostrarDatos(){

$('#divFoto').fadeTo('slow',.6);
$('#divFoto').append('<div style="position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;opacity:0.4;filter: alpha(opacity = 50)"></div>');

$('#divDatos').fadeTo('slow',1);

}