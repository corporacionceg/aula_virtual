$(document).ready(function() {
   
   tablaUsuarios();
   reparar();

   $('.tlf').mask('(####) ###-####');

   function reparar(){
      $("#tabUser tbody").on("click","#editar",function(){
         var tabla =$("table#tabUser").DataTable();
         var fila = tabla.row($(this).parents("tr")).data();
         //alert(fila.usuario);

         $.getJSON("../../views/usuarios/consultarUser.php",{usuario:fila.usuario},function(datos){
            if(datos != 0){              
                $.each(datos,function(K,V){
                  $("#usuario2").val(V['usuario']);
                  $("#telefono2").val(V['tlf']);
                  $("#correo2").val(V['correo']);
                  $("#tipo").val(V['tipo_user']);
                    //$("select#area").append("<option value="+V['id']+">"+V['descr']+"</option>");
                });
            }
            
         });

         $("#exampleModal").modal("show");
            
      });
   }

   $("#editUser").validate({
      rules: {
         tipo: {
            required: true,
         },
         usuario2: {
            required: true,
         },
      },
      messages: {
         tipo: {
            required: 'Seleccione el Tipo de Usuario',
         },
         usuario2: {
            required: 'El Usuario es requerido.',
         },
      },
      submitHandler: function() {
         parametros = $("#editUser").serialize();
         $.ajax({
               url: "../../Views/usuarios/guardarUser.php",
               type: "POST",
               data: parametros,
               success: function(data) {
                  if (data == 1) {
                     _Title = "¡Enhorabuena!";
                     _Text = "Transacción exitosa";
                     _Type = "success";

                     $("#exampleModal").modal("hide");

                     Swal.fire({
                        text: _Text,
                        title: _Title,
                        timer: 3000,
                        type: _Type,

                        onBeforeOpen: function() {
                           swal.showLoading()
                        }
                     }).then((result) => {
                        tablaUsuarios();
                     });

                  } else {
                     _Title = "ERROR!";
                     _Text = "Error en Transacción";
                     _Type = "error";

                     Swal.fire({
                        text: _Text,
                        title: _Title,
                        timer: 3000,
                        type: _Type,

                        onBeforeOpen: function() {
                           swal.showLoading()
                        }
                     }).then((result) => {});


                  }

               }
            })
            .fail(function() {
               swal("FATAL-ERROR", " ERROR DE AJAX :( :( ", "error");
            });
      }
   })


   $("form#NewUsers").validate({
      rules: {
         usuario: {
            required: true,
            maxlength: 12,
            minlength: 4

         },
         nombre: {
            required: true,
            maxlength: 18,
            minlength: 3
         },
         apellido: {
            required: true,
            maxlength: 18,
            minlength: 3
         },
         password: {
            required: true,
            maxlength: 18,
            minlength: 6
         },
         passwordRepeat: {
            equalTo: '#password',
            required: true,
            maxlength: 18,
            minlength: 6
         },
         correo: {
            required: true,
            maxlength: 30,
            minlength: 6
         },
      },
      messages: {
         usuario: {
            required: 'El Usuario es Requerido',
            maxlength: 'La Cantidad Maxina de caracteres es de 12 Digitos',
            minlength: 'La Cantidad Minima de caracteres es de 4 Digitos'
         },
         nombre: {
            required: 'El Nombre es Requerido',
            maxlength: 'La Cantidad Maxina de caracteres es de 18 Digitos',
            minlength: 'La Cantidad Minima de caracteres es de 3 Digitos'
         },
         apellido: {
            required: 'El Apellido es Requerido',
            maxlength: 'La Cantidad Maxina de caracteres es de 18 Digitos',
            minlength: 'La Cantidad Minima de caracteres es de 3 Digitos'
         },
         password: {
            required: 'La Contrasena es Requerida',
            maxlength: 'La Cantidad Maxina de caracteres es de 18 Digitos',
            minlength: 'La Cantidad Minima de caracteres es de 6 Digitos'
         },
         passwordRepeat: {
            equalTo: 'Las contraseña no coinciden',
            required: 'La Confirmacion es Requerida',
            maxlength: 'La Cantidad Maxina de caracteres es de 18 Digitos',
            minlength: 'La Cantidad Minima de caracteres es de 6 Digitos'
         },
         correo: {
            required: 'El Correo es Requerido',
            maxlength: 'La Cantidad Maxina de caracteres es de 18 Digitos',
            minlength: 'La Cantidad Minima de caracteres es de 6 Digitos'
         },
      },
      submitHandler: function() {
         parametros = $("form#NewUsers").serialize();
         $.ajax({
               url: "http://localhost/AulaVirtual/api/GuardarUsuario.php",
               type: "POST",
               data: parametros /*, id : equiposs*/ ,
               success: function(data) {
                  if (data == 1) {
                     _Title = "¡Enhorabuena!";
                     _Text = "Transacción exitosa";
                     _Type = "success";

                     Swal.fire({
                        text: _Text,
                        title: _Title,
                        timer: 3000,
                        type: _Type,

                        onBeforeOpen: function() {
                           swal.showLoading()
                        }
                     }).then((result) => {
                        tablaUsuarios();
                         $("#NewUsers")[0].reset();
                     });

                  } else {
                     _Title = "ERROR!";
                     _Text = "Error en Transacción";
                     _Type = "error";

                     Swal.fire({
                        text: _Text,
                        title: _Title,
                        timer: 3000,
                        type: _Type,

                        onBeforeOpen: function() {
                           swal.showLoading()
                        }
                     }).then((result) => {});


                  }

               }
            })
            .fail(function() {
               swal("FATAL-ERROR", " ERROR DE AJAX :( :( ", "error");
            });
      }
   });

});


function tablaUsuarios() {
   
   $('#tabUser').DataTable({
      "language": {
         "sProcessing": "Procesando...",
         "sLengthMenu": "Mostrar _MENU_ registros",
         "sZeroRecords": "No se encontraron resultados",
         "sEmptyTable": "Ningún dato disponible en esta tabla",
         "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
         "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
         "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
         "sInfoPostFix": "",
         "sSearch": "Buscar:",
         "sUrl": "",
         "sInfoThousands": ",",
         "sLoadingRecords": "Cargando...",
         "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
         },
         "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
         }
      },
      "destroy": true,
      "responsive": true,
      "paging": true,
      "autoWidth": false,
      // "dom": '<"top"lf>rt<"bottom"pi><"clear">',

      "ajax": "../../api/listaUsuarios.php",
      "columns": [ {
         "render": function(data, type, row) {
            return (row["nombre"] + ' ' + row["apellido"]);

         }
      }, {
         "data": "correo"
      }, {
         "data": "tlf"
      },{"defaultContent":"<button id='editar' class='btn btn-info'> Editar</button>"},
      {
         "render": function(data, type, row) {
            if (row["curriculo"] != ''  ) {

                  return '<a class="btn btn-xs btn-success" href="download.php?file='+row["curriculo"]+'" ><i class="fas fa-id-card"></i>&nbsp;Descargar CV</a>'; 


            }else{
                  return "<button class='btn btn-danger'>sin CV</button>"; 

            }
           

         }
      }
       ],
      //   "columnDefs": [
      //   {"className": "dt-center", "targets": "_all"}
      // ]
   });

}


function ValidarUsuario(){

   var usuario = $('#usuario').val();

   $.ajax({
      url: 'http://localhost/AulaVirtual/api/ValidarUsuario.php',
      type: 'POST',
      data: {usuario:usuario},
      dataType: 'json',
      success: function(data) {
         $('#labelusuario').removeAttr('class');
         $('#labelusuario').addClass(data.class);
         $('#labelusuario').html(data.mensaje);
         if (data.boton == 'true') {
            $('button[type="submit"]').removeAttr('disabled');
         }else{
            $('button[type="submit"]').attr('disabled','disabled');
         }
       }
   })
   .done(function() {
      console.log("success");
   })
   .fail(function() {
      console.log("error");
   })
   .always(function() {
      console.log("complete");
   });
   








}