$(document).ready(function() {

 PaginarCursos(1);

    $('#selectArea').select2({
        placeholder: 'Selecciona una Area',
        theme: 'bootstrap4',
           ajax: {
          url: '../../api/consultaAreas.php?main=1',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      });



  $("#selectArea").change(function () {
       if ( $("#selectArea").val() == 0) {
	PaginarCursos(1);

   }else{
	PaginarCursos(2);
   }
 	
  });
  $("#buscar").keypress(function () {
   if ( $("#selectArea").val() == 0 || $("#selectArea").val() == ''  || $("#selectArea").val() == null) {
	PaginarCursos(3);

   }else{
	PaginarCursos(4);
   }
 	
  });
});
 
 function PaginarCursos(param){

switch(param) {
  case 1:
    $.ajax({
  url: '../../api/mostrarCursos.php',
  type: 'POST',
  dataType: 'HTML',
  data: {vista: 1},
  success: function(data) {
      $('#DivCursos').html(data);
  }
});
    break;
  case 2:
    var categoria = $("#selectArea").val();

    $.ajax({
  url: '../../api/mostrarCursos.php',
  type: 'POST',
  dataType: 'HTML',
  data: {vista: 2,categoria:categoria},
  success: function(data) {
      $('#DivCursos').html(data);
  }
});

    break;
      case 3:
    var titulo = $("#buscar").val();

    $.ajax({
  url: '../../api/mostrarCursos.php',
  type: 'POST',
  dataType: 'HTML',
  data: {vista: 3,titulo:titulo},
  success: function(data) {
      $('#DivCursos').html(data);
  }
});

    break;
      case 4:
    var categoria = $("#selectArea").val();
    var titulo = $("#buscar").val();
    $.ajax({
  url: '../../api/mostrarCursos.php',
  type: 'POST',
  dataType: 'HTML',
  data: {vista: 4,categoria:categoria,titulo:titulo},
  success: function(data) {
      $('#DivCursos').html(data);
  }
});

    break;
  default:
    // code block
}

 }

 function verVideo(id){

    var user =$('#idusuario').val();

    $.getJSON('../../api/consultarInsCursos.php', {idCurso: id,user:user}, function(datos) {
        if (datos == 1) {
             window.location="http://localhost/AulaVirtual/Views/cursos/verCursoUsuario.php?curso="+id;
        }else{
          Swal.fire({
            title: 'Esta Seguro Que Desea Inscribirse en el curso ?',
            text: "Seria Una Gran Oportunidad Para Adquirir Nuevos Conocimientos.",
            icon: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Acepto!',
            cancelButtonText: 'No, cancelar!',
          }).then((result) => {
              if (result.value) {
                var usuario = $('#idusuario').val();
                $.ajax({
                  async:true,
                  url: '../../api/historico.php',
                  type: 'POST',
                  dataType: 'json',
                  data: {curso:id,usuario:usuario}
                });

                window.location="http://localhost/AulaVirtual/Views/cursos/verCursoUsuario.php?curso="+id;
              }
          });
        }
    });


 }
 function check(id){


var Label = $('#customSwitch1').prop("checked");
if (Label == true ) {

$.ajax({
  url: '../../api/visto.php',
  type: 'POST',
  dataType: 'JSON',
  data: {id:id,visto:1},
}).done(function() {

$('#actividades').show();

})
.fail(function() {
  console.log("error");
})
.always(function() {
  console.log("complete");
});



}else{

$.ajax({
  url: '../../api/visto.php',
  type: 'POST',
  dataType: 'JSON',
  data: {id:id,visto:0},
}).done(function() {
  console.log("success");
})
.fail(function() {
  console.log("error");
})
.always(function() {
  console.log("complete");
});

}

}