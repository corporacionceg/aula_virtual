    

    $.getJSON("consultarArea.php",function(datos){
        if(datos != 0){
            $("select#area").append("<option value='' selected>Seleccione</option>");

            $.each(datos,function(K,V){
                $("#area").append("<option value="+V['id']+">"+V['descr']+"</option>");
            });
        }else{
            $("select#area").empty();
        }
    });

    $.getJSON("consultarCurso.php",function(datos){
        if(datos != 0){
            $("select#curso").append("<option value='' selected>Seleccione</option>");

            $.each(datos,function(K,V){
                $("#curso").append("<option value="+V['id']+">"+V['descr']+"</option>");
            });
        }else{
            $("select#curso").empty();
        }
    });

    

    var tabla =$("table#TabPublic").DataTable();
    $("table#TabPublic tbody").on("click","button.comentarios",function(){
        var fila = tabla.row($(this).parents("tr")).data();
        var id=fila.id_publicacion;
        $("h4.modal-title").html("COMENTARIOS-ID: "+fila.id_publicacion);
    
        $("table#TblComentarios").DataTable().destroy();
        $("table#TblComentarios").DataTable({
            "ajax":{
              "type":"POST",
              "data":{"idPublicacion" :fila.id},
              "url":"../../views/consulta/consultaComentarios.php"
            },
            "columns":[
            
              {"data":"comentario"},
              {"data":"titulo"},
              {"data":"usuario"},
              {"data":"fecha"}
            ],
            "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
            },
              "oAria": {
                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            },
            "destroy":true,
        }).ajax().reload();

      $("#M_registrar").modal("show");
    });
    