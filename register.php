<?php

require_once("config/login.php");

$login = new Login();


if ($login->isUserLoggedIn() == true) {

   header("location: views/main/");

} else {
 define('RUTA','http://localhost/AulaVirtual/public/'); 
    ?>
<!DOCTYPE html>
<html lang="en">

<head>

 <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>SB Admin 2 - Register</title>
    <script src="public/js/popper.min.js"></script>
    <script src="public/js/jquery-3.4.1.min.js"></script>
    <script src="public/js/bootstrap.min.js"></script>
    <script src="public/js/jquery.validate.js"></script>
    <script src="public/js/additional-methods.js"></script>
    <script src="public/js/sweetalert2.js"></script>
    <script src="public/js/jquery.mask.min.js"></script>
    
    <script src="public/js/all.js"></script>
    <script src="public/js/jquery.dataTables.min.js"></script>
     
    <link rel="stylesheet" href="public/css/bootstrap.min.css">
    <link rel="stylesheet" href="public/css/all.css">
    <link rel="stylesheet" href="public/css/sweetalert2.css">
    <link rel="stylesheet" href="public/css/default.css">
    <link rel="stylesheet" href="public/css/jquery.dataTables.min.css">
 	<link href="<?php echo RUTA; ?>css/sb-admin-2.css" rel="stylesheet">

<script src="public/js/java.js"></script>
</head>

<body class="bg-gradient-primary">

  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
        <!--   <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
              </div>
              <form id="NewUsers" method="POST">
              	                 <div class="form-group">
                  <div  id="labelusuario" >
                    
                  </div>
                  <input type="text" onkeyup="ValidarUsuario();" class="form-control form-control-user" id="usuario" name="usuario"  placeholder="Usuario">
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                     <input type="text" class="form-control form-control-user" id="nombre" name="nombre" placeholder="Nombre">
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-user" id="apellido" name="apellido" placeholder="Apellido">
                  </div>
                </div>
                                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="Contraseña">
                  </div>
                  <div class="col-sm-6">
                    <input type="password" class="form-control form-control-user" id="passwordRepeat" name="passwordRepeat" placeholder="Repetir contraseña">
                  </div>
                </div>
                <div class="form-group">
                   <input type="email" class="form-control form-control-user" id="correo" name="correo" placeholder="Correo">
                </div>
                             <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="tel" class="form-control form-control-user tlf"  placeholder="Telefono" name="tlf" maxlength="15">
                  </div>
            </div>

<button type="submit"  class="btn btn-primary btn-user btn-block">Guardar</button> 
                <hr>
<!--                 <a href="index.html" class="btn btn-google btn-user btn-block">
                  <i class="fab fa-google fa-fw"></i> Register with Google
                </a>
                <a href="index.html" class="btn btn-facebook btn-user btn-block">
                  <i class="fab fa-facebook-f fa-fw"></i> Register with Facebook
                </a> -->
              </form>
              <hr>
              <div class="text-center">
               <!--  <a class="small" href="forgot-password.html">Forgot Password?</a> -->
              </div>
              <div class="text-center">
                <a class="small" href="index.php">Ya tienes una cuenta? Ingresa!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>


</body>

</html>
<?php } ?>